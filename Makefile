# Location of trees.
SOURCE_DIR  := modules
OUTPUT_DIR  := classes
DOCKER_PATH := /usr/src/myapp
LIBS        := libs

PACKAGE     := be.sakisan.tools
MAIN_CLASS  := $(PACKAGE).Jhoogle

# unix tools
FIND        := /usr/bin/find

DOCKER      := docker run --rm                                       \
                -u `id -u`                                           \
                -v "$$PWD":$(DOCKER_PATH)                            \
                -w $(DOCKER_PATH)                                    \
                openjdk:11 
JAVAC       := $(DOCKER) javac                                       \
                -d $(DOCKER_PATH)/$(OUTPUT_DIR)                      \
                --module-source-path $(DOCKER_PATH)/$(SOURCE_DIR)
JAVA        := $(DOCKER) java                                        \
                --module-path $(DOCKER_PATH)/$(OUTPUT_DIR) 
JAR         := $(DOCKER) jar                                         \
                --create                                             \
                --file $(DOCKER_PATH)/$(LIBS)/client.jar             \
                --main-class $(MAIN_CLASS)                           \
                -C $(DOCKER_PATH)/$(OUTPUT_DIR)/$(PACKAGE) .


# all_javas - Temp file for holding source file list
all_javas := $(OUTPUT_DIR)/all.javas

# compile - Compile the source
.PHONY: compile
compile: $(all_javas)
	$(JAVAC) @$<

# all_javas - Gather source file list
.INTERMEDIATE: $(all_javas)
$(all_javas):
	mkdir -p $(OUTPUT_DIR)
	$(FIND) $(SOURCE_DIR) -name '*.java' > $@

.PHONY: run
run:
	$(JAVA) --module $(PACKAGE)/$(MAIN_CLASS) int/Set

.PHONY: package
package:
	mkdir -p $(LIBS)
	$(JAR) 
