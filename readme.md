Jhoogle
- inspired by hoogle (haskell)
- [Stackoverflow: How to search for Java API methods by type signature?](https://stackoverflow.com/questions/4188919/how-to-search-for-java-api-methods-by-type-signature)
- [java docs](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/reflect/package-summary.html)


script usage e.g. `~/bin/jhoogle`
```
#!/bin/bash

docker run --rm -v ~/Projects/java/tools:/tools openjdk:11 \
     java -jar /tools/libs/client.jar "$@"
```
