package be.sakisan.tools;

import java.lang.reflect.*;
import java.util.*;

public class MethodFinder {

    public final Class<?> klass;

    /**
     * Constructs the method finder (doh)
     * @param klass the class
     */
    public MethodFinder(Class<?> klass) {
        this.klass = klass;
    }

    /**
     * Finds instance method matches
     * @param returnType the return type
     * @param arguments the arguments (in any order)
     * @return
     */
    public List<Method> findInstanceMethods(Class<?> returnType, 
            Class<?>... arguments) {

        List<Method> matches = new LinkedList<Method>();

        for (Method method : klass.getMethods()) {
            if ((method.getModifiers() & Modifier.STATIC) == 0) 
                if (testMethod(method, returnType, arguments))
                    matches.add(method);
        }

        return matches;        
    }

    /**
     * Finds static method matches
     * @param returnType the return type
     * @param arguments the arguments (in any order)
     * @return
     */
    public List<Method> findStaticMethods(Class<?> returnType,
            Class<?>... arguments) {

        List<Method> matches = new LinkedList<Method>();

        for (Method method : klass.getMethods()) 
            if ((method.getModifiers() & Modifier.STATIC) != 0) 
                if (testMethod(method, returnType, arguments))
                    matches.add(method);

        return matches;        
    }

    /**
     * Tests a method if it is a match
     * @param method the method to test
     * @param returnType the return type
     * @param arguments the arguments (in any order)
     * @return true if it matches
     */
    private boolean testMethod(Method method, 
            Class<?> returnType, 
            Class<?>... arguments) {

        boolean returnTypeIsOk = false;
        for (Class<?> ic : getInterchangable(returnType))
            if (ic.isAssignableFrom(method.getReturnType()))
                returnTypeIsOk = true;

        if (!returnTypeIsOk)
            return false;

        Class<?>[] methodArguments = method.getParameterTypes();

        if (methodArguments.length != arguments.length)
            return false;

        if (methodArguments.length == 0) {
            return true;
        }
        else {
            Permutations permutations = new Permutations(arguments);

            outer: for (Class<?>[] permutation : permutations) {
                for (int i = 0; i < methodArguments.length; i++) {

                    boolean canAssign = false;
                    for (Class<?> ic : getInterchangable(permutation[i])) 
                        if (methodArguments[i].isAssignableFrom(ic))
                            canAssign = true;

                    if (!canAssign)
                        continue outer;
                }
                return true;
            }

            return false;
        }
    }

    /**
    * Returns the autoboxing types
    * @param type the type to autobox :)
    * @return a list of types that it could be
    */
    private static Class<?>[] getInterchangable(Class<?> type) {

        if (type == Boolean.class || type == Boolean.TYPE)
            return new Class<?>[] { Boolean.class, Boolean.TYPE };
        if (type == Character.class || type == Character.TYPE)
            return new Class<?>[] { Character.class, Character.TYPE };
        if (type == Short.class || type == Short.TYPE)
            return new Class<?>[] { Short.class, Short.TYPE };
        if (type == Integer.class || type == Integer.TYPE)
            return new Class<?>[] { Integer.class, Integer.TYPE };
        if (type == Float.class || type == Float.TYPE)
            return new Class<?>[] { Float.class, Float.TYPE };
        if (type == Double.class || type == Double.TYPE)
            return new Class<?>[] { Double.class, Double.TYPE };
        if (type == Void.class || type == Void.TYPE)
            return new Class<?>[] { Void.class, Void.TYPE };

        return new Class<?>[] { type };
    }


    /**
    * Creates a permutation list of all different combinations
    */
    @SuppressWarnings("serial")
    private class Permutations extends LinkedList<Class<?>[]> {

        /**
        * Creates a permutation list
        * @param list the list to be permutated
        */
        public Permutations(Class<?>[] list) {
            permutate(new LinkedList<Class<?>>(Arrays.asList(list)),
                    new LinkedList<Class<?>>());
        }

        // ugly, there is better ways of doing this...
        private void permutate(List<Class<?>> tail, List<Class<?>> choosen) {

            if (tail.isEmpty()) {
                add(choosen.toArray(new Class<?>[0]));
                return;
            }

            ListIterator<Class<?>> it = tail.listIterator();

            while (it.hasNext()) {

                Class<?> current = it.next();

                choosen.add(current);
                it.remove();

                permutate(new LinkedList<Class<?>>(tail), choosen);

                choosen.remove(current);
                it.add(current);
            }
        }
    }
}
