package be.sakisan.tools;

import java.io.File;
import java.io.FileInputStream;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.Properties;

public class LoadClasses {

    private Set<String> disallowedPackages = new HashSet<>();

    public LoadClasses() {
        final String userHome = System.getProperty("user.home");
        if (!"?".equals(userHome)) {
            final String configFilePath = userHome
                + File.separator + ".config"
                + File.separator + "jhoogle"
                + File.separator + "excluded-packages.properties";
            try {
                Properties config = new Properties();
                config.load(new FileInputStream(configFilePath));
                for (Object packageKey : config.keySet()) {
                    if (packageKey instanceof String) {
                        disallowedPackages.add((String) packageKey);
                    }
                }
            } catch (java.io.FileNotFoundException e) {
                System.out.println("Hint: you can exclude packages in " + configFilePath);
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
    }

    public List<MethodFinder> loadClasses() {
        List<MethodFinder> methodFinders = new ArrayList<>();
        try{
            FileSystem jrt = FileSystems.newFileSystem(URI.create("jrt:/"), new HashMap<>());
            Files.walk(jrt.getPath("/")).forEach(p -> {
                String name = p.toString();
                if (name.endsWith(".class") && !name.endsWith("module-info.class")) {
                    String className = getClassName(name);
                        if (allowed(className)) {
                        try{
                            // System.out.println(className);
                            Class<?> classForName = Class.forName(className);
                            methodFinders.add(new MethodFinder(classForName));
                        } catch(Exception e) { /*System.out.println(e.getMessage());*/ }
                    }
                }
            });
        } catch(Exception e) { throw new RuntimeException(e); }
        return methodFinders;
    }

    private boolean allowed(String name) {
        return !name.startsWith("sun.")
            && !(name.startsWith("com.sun.") && name.contains(".internal."))
            && !(name.startsWith("jdk.internal."))
            && !(name.startsWith("jdk.vm.ci.hotspot"))
            && !(name.equals("jdk.vm.ci.runtime.JVMCI"))
            && !(name.startsWith("org.graalvm.compiler."))
            && !disallowed(name)
            ;
    }

    private boolean disallowed(String name) {
        return disallowedPackages.stream()
            .anyMatch(key -> name.startsWith(key));
    }

    private String getClassName(String path) {
        return path.substring(path.indexOf('/', 10) + 1, path.length() - 6)
            .replaceAll("/",".");
    }
}
