package be.sakisan.tools;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;
import java.io.*;

/**
 * Original code: https://stackoverflow.com/a/4203354/3967814
 */
public class Jhoogle {

    private final List<MethodFinder> klasses = new LoadClasses().loadClasses();

    public static void main(String[] args) {
        new Jhoogle().run(args);
    }

    public void run (String[] args) {
        List<Class<?>> types = getTypes(args);
        printExampleSearch(types.get(0), types.subList(1,types.size()));
        // printExampleSearch(Integer.class, new int[0].getClass());
        // printExampleSearch(String.class, String.class, Character.class, Character.class);
        // printExampleSearch(Integer.class, String.class);
        // printExampleSearch(Void.class, List.class);
    }

    public List<Class<?>> getTypes(String[] args) {
        String[] tokens = String.join("/", Arrays.asList(args)).split("/");
        return Stream.of(tokens)
            .map(String::trim)
            .filter(s -> s.length() > 0)
            .map(token -> toClass(token))
            .collect(Collectors.toList());
    }

    public Class<?> toClass(String token) {
        if("int".equalsIgnoreCase(token)) {
            return Integer.class;
        } 
        if("string".equalsIgnoreCase(token)) {
            return String.class;
        } 
        if("char".equalsIgnoreCase(token)) {
            return Character.class;
        } 
        if("list".equalsIgnoreCase(token)) {
            return List.class;
        } 

        for(MethodFinder finder : klasses) {
            if (token.equalsIgnoreCase(finder.klass.getName())){
                return finder.klass;
            }
        }

        for(MethodFinder finder : klasses) {
            if (token.equalsIgnoreCase(finder.klass.getSimpleName())){
                return finder.klass;
            }
        }

        return Object.class;
    }

   public void printExampleSearch(Class<?> returnType, List<Class<?>> types) {

        for (int i = 0; i < types.size(); i++) {
            System.out.print((i == 0 ? "":", ") + types.get(i).getName());
        }

        System.out.println(" -> " + returnType.getName());

        Set<Method> methods = findMethods(returnType, types.toArray(new Class<?>[]{}));

        for (Method method : methods) {
            String methodString = method.toString().replaceAll("java\\.lang\\.","");
            System.out.println("\t" + methodString);
        }

        System.out.println();
    }


    /**
     * Finds a set of methods
     * @param returnType the return type
     * @param arguments the arguments (in any order)
     * @return a set of methods
     */
    public Set<Method> findMethods(Class<?> returnType, Class<?>... arguments) {

        Set<Method> methods = new LinkedHashSet<Method>();

        if (arguments.length > 0) {
            MethodFinder instance = new MethodFinder(arguments[0]);

            Class<?>[] rest = new Class<?>[arguments.length - 1];
            System.arraycopy(arguments, 1, rest, 0, rest.length);

            methods.addAll(instance.findInstanceMethods(returnType, rest));
        }
        else {
            for (MethodFinder k : klasses)
                methods.addAll(k.findInstanceMethods(returnType, arguments));
        }

        for (MethodFinder k : klasses)
            methods.addAll(k.findStaticMethods(returnType, arguments));

        return methods;
    }

}


